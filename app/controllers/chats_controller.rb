class ChatsController < ApplicationController
  def show
    if logged_in?
      @messages = Message.all
    else
      redirect_to root_path
      flash[:danger] = "You must be logged to enter the chat room"
    end
  end
end
